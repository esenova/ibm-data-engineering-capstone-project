# **OLTP Database**

Estimated time needed: **30** minutes.

# **About This SN Labs Cloud IDE**

This Skills Network Labs Cloud IDE provides a hands on environment for course and project related labs. It utilizes Theia, an open-source IDE (Integrated Development Environment) platform, that can be run on desktop or on the cloud. To complete this lab, we will be using the Cloud IDE based on Theia and MySQL running in a Docker container.

# **Scenario**

You are a data engineer at an e-commerce company. Your company needs you to design a data platform that uses MySQL as an OLTP database. You will be using MySQL to store the OLTP data.

## **Objectives**

In this assignment you will:

- design the schema for OLTP database.
- load data into OLTP database.
- automate admin tasks.

# **Tools/Software**

- MySQL 8.0.22
- phpMyAdmin 5.0.4

# **Note - Screenshots**

Throughout this lab you will be prompted to take screenshots and save them on your own device. You will need these screenshots to either answer graded quiz questions or to upload as your submission for peer review at the end of this course. You can use various free screengrabbing tools to do this or use your operating system's shortcut keys to do this (for example Alt+PrintScreen in Windows).

# **Assignment Overview**

In this assignment, you will perform three exercises with multiple tasks. But before proceeding with the assignment, you will check the lab environment by starting the MySQL server and then downloading the file database from the given link. The first exercise requires you to design the schema for the OLTP database by storing data like row ID, product ID, customer ID, quantity, and time stamp of sale.

In the second exercise, you will load this data into the OLTP database by important the data in the downloaded file and then listing the tables in the database. You will also write a query to find out the count of records in the tables. In the final exercise, you will automate admin tasks by writing a query to list all the records created in the last 24 hours and then export them to a file. You will also write a bash script that exports records created in the last 24 hours into another file. After performing each task, you will take a screenshot of the command used, and the output obtained and give a name to the screenshot. 


